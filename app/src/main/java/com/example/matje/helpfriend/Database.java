package com.example.matje.helpfriend;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Matje on 13.07.2017.
 */

public class Database extends SQLiteOpenHelper {
    public Database(Context context) {
        super(context, "contact.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table contact(" +
                        "id integer primary key autoincrement," +
                        "name text," +
                        "numer text);" +
                        "");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


    public void addContact(Data data){

        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name", data.getName());
        values.put("numer", data.getNumer());
        db.insertOrThrow("contact",null, values);
    }

    public List<Data> getAll(){
        List<Data> contacts = new LinkedList<Data>();
        String[] columns={"name","numer"};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor =db.query("contact",columns,null,null,null,null,null,null);
        while(cursor.moveToNext()){
            Data data = new Data();
            data.setName(cursor.getString(0));
            data.setNumer(cursor.getString(1));
            contacts.add(data);
        }
        return contacts;
    }

    public String getNumer(String name){
        SQLiteDatabase db = getReadableDatabase();
        String[] columns={"numer"};
        String args[]={name+""};
        Cursor cursor=db.query("contact",columns,"name=?",args,null,null,null,null);
        String numer=null;
        if(cursor!=null){
            cursor.moveToFirst();
            numer = cursor.getString(0);
        }
        return numer;
    }

    public void deleteKey(String numer){
        SQLiteDatabase db = getWritableDatabase();
        db.delete("contact","numer" + "=" + numer, null);
    }

    public int getID(String numer) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor mCursor = db.rawQuery(
                "SELECT id  FROM  contact WHERE numer='"+numer+"'" , null);

        mCursor.moveToFirst();
        return mCursor.getInt(0);
    }



}
