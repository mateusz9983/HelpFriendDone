package com.example.matje.helpfriend;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;

public class Glowna extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED){
            Log.d("HelpFriend", "Permission is not granted, requesting");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, 123);
        } else {
            Log.d("HelpFriend", "Permission is granted");
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        }
        Button select=(Button)findViewById(R.id.BTNStart);
        Animation mAnimation = new AlphaAnimation(1, 0);
        mAnimation.setDuration(200);
        mAnimation.setInterpolator(new LinearInterpolator());
        mAnimation.setRepeatCount(Animation.INFINITE);
        mAnimation.setRepeatMode(Animation.REVERSE);
        select.startAnimation(mAnimation);

        select.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable(){
                    public void run(){
                        startActivity(new Intent(Glowna.this, Send_sms.class));
                    }
                }).start();


            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }


    public boolean onCreateOptionsMenu(Menu main) {
        getMenuInflater().inflate(R.menu.main, main);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog alertDialog = new AlertDialog.Builder(Glowna.this).create();
        switch (item.getItemId()) {
            case R.id.p1:
                alertDialog.setTitle("O autorze");
                alertDialog.setMessage("Autorem aplikacji jest Mateusz Dworak.\nPomysłodawcą aplikacji jest Michał Brutt. Pozdrawiam Michała!\nKontakt do mnie: mateusz1dworak@gmail.com.");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                return true;
            case R.id.p2:
                alertDialog.setTitle("Informację");
                alertDialog.setMessage("Ikony pobrane ze strony https://icons8.com.\nLokalizacja jest realizowana za pomocą GoogleApiClient.");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                return true;
            case R.id.p3:
                alertDialog.setTitle("O aplikacji");
                alertDialog.setMessage("Aplikacja umożliwia pobranie aktualnej lokalizji oraz wysłanie jej do wybranych wcześniej osób z listy kontaktów.\n W wersji  androida mniejszych niż 6 za każdym razem przy wysyłaniu wiadomości aplikacja będzie prosiła o pozwolenie. W ustawieniach należy pozwolić na bezpośrednie wysyłanie wiadmości przez aplikację HelpFriend.Również w tych wersjach postęp ustalania lokalizacji jest widoczny na pasku powiadomień.\n" +
                        "W zależności gdzie się znajdujesz ustalanie lokalizacji może trwać do minuty. Zazwyczaj jest to 10-15 sekund. W wersji androida 6 oraz wyższej punkt na mapie zostanie dodany, gdy lokalizacja zostanie ustalona.");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_conf) {
            final ProgressDialog dialog = new ProgressDialog(Glowna.this);
            dialog.setMessage("Wczytuje kontakty...");
            dialog.show();
            new Thread(new Runnable(){
                public void run(){
                    finish();
                    startActivity(new Intent(Glowna.this, Configuration.class));
                    dialog.dismiss();
                }
            }).start();

        } else if (id == R.id.nav_doneContacts) {
            final ProgressDialog dialog = new ProgressDialog(Glowna.this);
            dialog.setMessage("Wczytuje kontakty...");
            dialog.show();
            new Thread(new Runnable(){
                public void run(){
                    startActivity(new Intent(Glowna.this, DoneList.class));
                    dialog.dismiss();
                }
            }).start();
        } else if (id == R.id.nav_run) {
            final ProgressDialog dialog = new ProgressDialog(Glowna.this);
            dialog.setMessage("Uruchamiam...");
            dialog.show();
            new Thread(new Runnable(){
                public void run(){
                    startActivity(new Intent(Glowna.this, Send_sms.class));
                    dialog.dismiss();
                }
            }).start();
        }
        else if(id == R.id.nav_whereIam) {
            final ProgressDialog dialog = new ProgressDialog(Glowna.this);
            dialog.setMessage("Szukam lokalizacji...");
            dialog.show();
            new Thread(new Runnable(){
                public void run(){
                    startActivity(new Intent(Glowna.this, MapsActivity.class));
                    dialog.dismiss();
                }
            }).start();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Glowna.this);
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            alertDialog.setTitle("Wyjście");
            alertDialog.setMessage("Czy na pewno chcesz zakończyć działanie aplikacji ?");
            alertDialog.setPositiveButton("Tak",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            return;
                        }
                    }) ;
            alertDialog.setNegativeButton("Nie",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
