package com.example.matje.helpfriend;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Matje on 08.08.2017.
 */

public class Send_smsCopy extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, OnMapReadyCallback {
    LocationRequest locationRequest;
    private Database handler;
    final Database db = new Database(this);
    public static final String TAG = MapsActivity.class.getSimpleName();
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private SharedPreferences preferences;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);
        preferences = getSharedPreferences("WSP", Activity.MODE_PRIVATE);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.iconback);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.SEND_SMS}, 123);
            }
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(2 * 1000); // 1 second, in milliseconds


        handler = new Database(getApplicationContext());
        List<Data> Lista;
        Lista = handler.getAll();
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);

        final String[] ListaDone = new String[Lista.size()];

        for (int i = 0; i < Lista.size(); i++) {
            String dana = Lista.get(i).getName().toString() + " " + Lista.get(i).getNumer().toString();
            ListaDone[i] = dana;
        }
        TextView t1 = (TextView) findViewById(R.id.Emptylist);
        Button BtnAdd = (Button) findViewById(R.id.BtnAdd);
        if (ListaDone.length == 0) {
            t1.setVisibility(View.VISIBLE);
            BtnAdd.setVisibility(View.VISIBLE);

        } else {
            t1.setVisibility(View.INVISIBLE);
            BtnAdd.setVisibility(View.INVISIBLE);
        }
        BtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog dialog = new ProgressDialog(Send_smsCopy.this);
                dialog.setMessage("Wczytuje kontakty...");
                dialog.show();
                new Thread(new Runnable() {
                    public void run() {
                        startActivity(new Intent(Send_smsCopy.this, Configuration.class));
                        dialog.dismiss();
                    }
                }).start();
            }
        });

        final ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.activity_listview_sendsms, ListaDone);
        final ListView listView = (ListView) findViewById(R.id.ContactDoneList);
        adapter.sort(new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {

                return (lhs).compareTo(rhs);
            }
        });
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int itemPosition = position;
                final String itemValue = (String) listView.getItemAtPosition(position);
                if ((ActivityCompat.checkSelfPermission(Send_smsCopy.this, android.Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(Send_smsCopy.this, android.Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions(Send_smsCopy.this, new String[]{android.Manifest.permission.SEND_SMS}, 123);
                    ActivityCompat.requestPermissions(Send_smsCopy.this, new String[]{android.Manifest.permission.INTERNET}, 123);
                    Log.d("HelpFriend", "Permission is granted1");

                }

                //else {
                Log.d("HelpFriend", "Permission is not granted, requesting1");
                try {

                     Log.i("HF","Zapytanie SMS_send_copy");
                        String[] separated = itemValue.split(":");
                        final String mes = preferences.getString("wsp", "");
                        if (mes != "") {
                            String nazwa = separated[0];
                            final String numer = separated[1];
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String[] wsp = mes.split(":");
                                    if (ActivityCompat.checkSelfPermission(Send_smsCopy.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Send_smsCopy.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                        ActivityCompat.requestPermissions(Send_smsCopy.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 123);
                                        ActivityCompat.requestPermissions(Send_smsCopy.this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, 123);
                                    }
                                    String sms = "Moja przybliżona lokalizacją to: http://maps.google.com/maps?q=" + wsp[0] + "," + wsp[1] + "(" + "Jestem+Tutaj" + ")&z=15";
                                    SmsManager sm = SmsManager.getDefault();
                                    ArrayList<String> parts = sm.divideMessage(sms);
                                    int numParts = parts.size();

                                    ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
                                    ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();

                                    for (int i = 0; i < numParts; i++) {
                                        sentIntents.add(PendingIntent.getBroadcast(getApplicationContext(), 0, getIntent(), 0));
                                        deliveryIntents.add(PendingIntent.getBroadcast(getApplicationContext(), 0, getIntent(), 0));
                                    }
                                    sm.sendMultipartTextMessage(numer, null, parts, sentIntents, deliveryIntents);
                                    Log.i("HelpFriend SendSMS", "wysłano");
                                }
                            }).start();
                            Toast.makeText(getApplicationContext(), "SMS został wysłany do " + nazwa, Toast.LENGTH_LONG).show();


                        } else {
                            Toast.makeText(getApplicationContext(), "Nie pobranno lokalizacji", Toast.LENGTH_LONG).show();

                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Nie można wysłać lub błąd z lokalizacją", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
               // }
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (LocationListener) Send_smsCopy.this);
            mGoogleApiClient.disconnect();
        }
    }
    private void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.putString("wsp",currentLatitude+":"+currentLongitude);
        preferencesEditor.commit();
        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
        Log.i("HF loc", String.valueOf(currentLatitude)+ String.valueOf(currentLongitude));


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.i("HelpFriend", "Mapy, brak uprawnienień");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 123);
            return;
        }
        else {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (location == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } else {
                handleNewLocation(location);
            }
            handleNewLocation(location);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {  /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, Glowna.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}


