package com.example.matje.helpfriend;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Matje on 11.07.2017.
 */

public class Configuration extends AppCompatActivity {

    private Database handler;

    final Database db= new Database(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conf);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.iconback);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        handler = new Database(getApplicationContext());
        String[] Lista=getContactList();
        final ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.activity_listview_add, Lista);
        final ListView listView = (ListView)findViewById(R.id.ContactList);
        adapter.sort(new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {

                return (lhs).compareTo(rhs);
            }
        });
        listView.setAdapter(adapter);
        listView.setTextFilterEnabled(true);
        final EditText findText = (EditText)findViewById(R.id.FindText) ;
        findText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);

            }
        });
        findText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findText.setText("");
            }
        });
        Button btnGetContact = (Button) findViewById(R.id.BTNAddContact);
        btnGetContact.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(Configuration.this, DoneList.class));
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int itemPosition     = position;
                String  itemValue    = (String) listView.getItemAtPosition(position);

                try {
                    String[] separated = itemValue.split(":");
                    String Numer=""; // numer bez spacji
                    String nazwa = separated[0];
                    String numer = separated[1];
                    String[] separatedNumer = numer.split(" ");
                    Data data= new Data();
                    if (separatedNumer.length==3) {
                        Numer+=separatedNumer[0];
                        Numer+=separatedNumer[1];
                        Numer+=separatedNumer[2];
                        data.setNumer(Numer);
                    }
                    else
                        data.setNumer(numer);
                    data.setName(nazwa+":");
                    List<Data> Lista;
                    Lista = handler.getAll();
                    boolean czydodany=false;
                    for( int i =0 ; i < Lista.size();i++){
                        //Toast.makeText(getApplicationContext(),
                              //  Lista.get(i).getNumer() + data.getNumer(), Toast.LENGTH_SHORT)
                               // .show();
                        if(Lista.get(i).getNumer().toString().equals(data.getNumer().toString())) {
                            czydodany=true;
                            break;
                        }
                    }
                    if(czydodany==true){
                        Toast.makeText(getApplicationContext(),
                                "Ten kontakt został już dodany. "+itemValue, Toast.LENGTH_SHORT)
                                .show();
                        findText.setText("");
                    }
                    else {
                        handler.addContact(data);
                        Toast.makeText(getApplicationContext(),
                                "Dodano: " + data.getName().toString()+ " "+data.getNumer().toString(), Toast.LENGTH_SHORT)
                                .show();
                        findText.setText("");
                    }
                }
                catch (Exception e)
                    {
                        Toast.makeText(getApplicationContext(),
                                "Nie można dodać elemetu: "+itemValue, Toast.LENGTH_LONG)
                                .show();
                   }
            }
        });


    }

    public String[] getContactList(){
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        Log.i("Log", "get Contact List: Fetching complete contact list");
        ArrayList<String> contact_names = new ArrayList<String>();
        if (cur.getCount() > 0)
        {
            while (cur.moveToNext())
            {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER.trim())).equalsIgnoreCase("1"))
                {
                    if (name!=null){
                        //contact_names[i]=name;

                        Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",new String[]{id}, null);
                        while (pCur.moveToNext())
                        {
                            String PhoneNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            PhoneNumber = PhoneNumber.replaceAll("-", "");
                            if (PhoneNumber.trim().length() >= 11) {
                                PhoneNumber = PhoneNumber.substring(PhoneNumber.length() - 11);
                            }
                            contact_names.add(name + ":" + PhoneNumber);

                            break;
                        }
                        pCur.close();
                        pCur.deactivate();
                    }
                }
            }
            cur.close();
            cur.deactivate();
        }

        String[] contactList = new String[contact_names.size()];

        for(int j = 0; j < contact_names.size(); j++){
            contactList[j] = contact_names.get(j);
        }

        return contactList;
    }
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                startActivity(new Intent(this, Glowna.class));
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent( this, Glowna.class));


    }



}