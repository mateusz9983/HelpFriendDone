package com.example.matje.helpfriend;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Matje on 13.07.2017.
 */

public class DoneList extends AppCompatActivity {
    private Database handler;
    final Database db= new Database(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done_list);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.iconhome);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Zaufane kontakty");
        handler = new Database(getApplicationContext());
        List<Data> Lista;
        Lista = handler.getAll();
        final String[] ListaDone = new String[Lista.size()];
        final ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.activity_listview_delete, ListaDone);
        final ListView listView = (ListView) findViewById(R.id.ContactDoneList);
        TextView t1 = (TextView)findViewById(R.id.Emptylist);
        Button BtnAdd= (Button)findViewById(R.id.BtnAdd);
        final Button Btn_delete = (Button) findViewById(R.id.BtnDelete);
        if(ListaDone.length==0){
            t1.setVisibility(View.VISIBLE);
            BtnAdd.setVisibility(View.VISIBLE);
            Btn_delete.setVisibility(View.INVISIBLE);
        }
        else {
            t1.setVisibility(View.INVISIBLE);
            BtnAdd.setVisibility(View.INVISIBLE);
            Btn_delete.setVisibility(View.VISIBLE);
        }
        BtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog dialog = new ProgressDialog(DoneList.this);
                dialog.setMessage("Wczytuje kontakty...");
                dialog.show();
                new Thread(new Runnable(){
                    public void run(){
                        startActivity(new Intent(DoneList.this, Configuration.class));
                        finish();
                        dialog.dismiss();
                    }
                }).start();
            }
        });
        for (int i = 0; i < Lista.size(); i++) {
            String dana = Lista.get(i).getName().toString() + " " + Lista.get(i).getNumer().toString();
            ListaDone[i] = dana;
        }

        adapter.sort(new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {

                return (lhs).compareTo(rhs);
            }
        });
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int itemPosition = position;
                final String itemValue = (String) listView.getItemAtPosition(position);
                try {
                    // ListView Clicked item value
                    String[] separated = itemValue.split(":");
                    String nazwa = separated[0];
                    final String numer = separated[1];
                    final List<Data> Lista;
                    Lista = db.getAll();
                    AlertDialog.Builder builder = new AlertDialog.Builder(DoneList.this,R.style.AppTheme_AlertDialog);
                    builder.setMessage("Czy na pewno chcesz usunąć " + nazwa + "?");
                    builder.setPositiveButton("  ", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                db.deleteKey(numer);
                                dialog.dismiss();
                            }catch (Exception e){
                                Toast.makeText(getApplicationContext(),
                                        "Nie można usunąć. Sprawdź poprawność numeru telefonu.", Toast.LENGTH_SHORT)
                                        .show();
                                return;
                            }

                            String[] ListaDone1 = new String[Lista.size()];
                            for (int i = 0; i < Lista.size(); i++) {
                                String dana = Lista.get(i).getName().toString() + " " + Lista.get(i).getNumer().toString();
                                ListaDone1[i] = dana;

                            }
                            TextView t1 = (TextView) findViewById(R.id.Emptylist);
                            Button BtnAdd = (Button) findViewById(R.id.BtnAdd);
                            if (ListaDone1.length == 0) {
                                t1.setVisibility(View.VISIBLE);
                                BtnAdd.setVisibility(View.VISIBLE);
                                Btn_delete.setVisibility(View.INVISIBLE);
                            } else {
                                Btn_delete.setVisibility(View.VISIBLE);
                                t1.setVisibility(View.INVISIBLE);
                                BtnAdd.setVisibility(View.INVISIBLE);
                            }
                            BtnAdd.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                    startActivity(new Intent(DoneList.this, Configuration.class));
                                }
                            });

                            final ArrayAdapter adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.activity_listview_add, ListaDone1);
                            final ListView listView = (ListView) findViewById(R.id.ContactDoneList);

                            adapter.sort(new Comparator<String>() {
                                @Override
                                public int compare(String lhs, String rhs) {

                                    return (lhs).compareTo(rhs);
                                }
                            });
                            Toast.makeText(getApplicationContext(),
                                    "Usunięto: " + itemValue, Toast.LENGTH_SHORT)
                                    .show();
                            finish();
                            startActivity(new Intent(DoneList.this, DoneList.class));

                        }
                    });
                    builder.setNegativeButton(" ", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();

                    alert.getButton(alert.BUTTON_POSITIVE).setBackground(getDrawable(R.drawable.iconstrue));
                    alert.getButton(alert.BUTTON_NEGATIVE).setBackground(getDrawable(R.drawable.iconsdeletedialog));
                    alert.getButton(alert.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.BLACK));
                    alert.getButton(alert.BUTTON_POSITIVE).setTextSize(30);
                    alert.getButton(alert.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.BLACK));
                    alert.getButton(alert.BUTTON_NEGATIVE).setTextSize(30);
                }

                catch (Exception e) {
                    Toast.makeText(getApplicationContext(),
                            "Nie można usunąć: " + itemValue, Toast.LENGTH_LONG)
                            .show();
                    finish();
                    startActivity(new Intent(DoneList.this, DoneList.class));
                }
                listView.setAdapter(adapter);


            }
        });


        Btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.close();
                getApplicationContext().deleteDatabase(db.getDatabaseName());
                Toast.makeText(getApplicationContext(),
                        "Wyszczono listę", Toast.LENGTH_SHORT)
                        .show();
                finish();
                startActivity(new Intent(DoneList.this, Glowna.class));
            }
        });
    }
    @Override
    public void onBackPressed() {
        final ProgressDialog dialog = new ProgressDialog(DoneList.this);
        dialog.setMessage("Wczytuje kontakty...");
        dialog.show();
        new Thread(new Runnable(){
            public void run(){
                finish();
                startActivity(new Intent(DoneList.this, Glowna.class));
                dialog.dismiss();
            }
        }).start();

    }
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                startActivity(new Intent(this, Glowna.class));
        }
        return super.onOptionsItemSelected(item);
    }

}




